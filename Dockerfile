FROM node:16

ENV NODE_ENV=production

WORKDIR /app

COPY ["src/package.json", "src/package-lock.json*", "/app/"]

RUN npm install --production

COPY src/ .

CMD [ "npm", "run", "start" ]