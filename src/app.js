const { Client, GatewayIntentBits, ActivityType, Events } = require('discord.js');
const fs = require("fs")

const config = require("./config");
const reactToRolesMentioned = require('./events/reactToRolesMentioned');

const client = new Client({ intents: [GatewayIntentBits.Guilds, GatewayIntentBits.GuildMessages, GatewayIntentBits.GuildMessageReactions] })

// Add all events files
const eventFiles = fs.readdirSync('./events').filter(file => file.endsWith('.js'));

for (const file of eventFiles) {
    const event = require(`./events/${file}`);
    if (event.once) {
        client.once(event.name, (...args) => event.execute(...args));
    } else {
        client.on(event.name, (...args) => event.execute(...args));
    }
}



const TOKEN = config.BOT_TOKEN();

client.login(TOKEN)

client.once('ready', () => {
    console.info(`Logged in as ${client.user.tag}!`)

    client.user.setPresence({
        activities: [{ name: `Brooklyn Nine-Nine`, type: ActivityType.Watching }],
        status: 'online',
    });

})

const handlerCache = {}

client.on(Events.InteractionCreate, interaction => {
    interaction.message.channel.messages.fetch(interaction.message.reference.messageId)
        .then(async (message) => {
            if (!interaction.isButton()) return;
            /**
             * This dynamically loads interaction handlers from the customId of the interaction
             * So for your own buttons use your handlers name (without "Handler") as prefix for the buttons customId
             * e.g. Yes button for reactionNotificationSetupHandler
             * ```
             * reactionNotificationSetup-YesButton
             */
            const handlerName = interaction.customId.split('-')[0]
            let handler = require(`./handler/${handlerName}Handler`);
            await handler.execute(interaction, message)
        })
        .catch(e => console.log(e))
});
