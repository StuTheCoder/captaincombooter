const config = require("../config")
const { MessageEmbed, DataResolver, EmbedBuilder, ActionRowBuilder, ButtonBuilder, ButtonStyle, Events, Embed } = require('discord.js');
const fs = require('fs');
const { getReportEmbed, getNotificationEmbed, askForReactionNotification } = require("../services/embedService");
const notificationService = require("../services/notificationService");

emojis = undefined
choosenEmojis = []
mentionedRoleId = undefined

blocklist = "./data/blocklist.json"

const buttonPrefix = "reactionNotificationSetup-"
const buttons = {
	yesButton: `${buttonPrefix}YesButton`,
	noButton: `${buttonPrefix}NoButton`,
	blockButton: `${buttonPrefix}BlockButton`
}


module.exports = {
	name: 'messageCreate',
	async execute(message) {
		// Filter opt-outed people
		if (fs.existsSync(blocklist)) {
			allBlockedUsers = JSON.parse(fs.readFileSync((blocklist)))
			if (allBlockedUsers.includes(message.author.id)) return
		}
		// Get id of mentioned role in a message
		// ToDo check all mentioned roles/not just the first
		mentionedRoleId = message.mentions.roles.keys().next().value
		const channelId = message.channelId

		const roleRules = await config.MENTION_ROLE_RULES(mentionedRoleId);

		// Check if rules are existing and channel id the set one from config
		if (!roleRules || channelId != roleRules.channelId) return

		emojis = roleRules.reactions

		// React with all emojis given in config
		roleRules.reactions.forEach((currentEmoji) => {
			message.react(currentEmoji)
		})

		// Ask for notification if enabled			
		if (config.NOTIFICATION_RULES.askForNotifications == false) return

		const selfdestructionTimer = config.NOTIFICATION_RULES().askForNotificationsTimout
		const embed = getNotificationEmbed(selfdestructionTimer)


		// Preparing buttons for embed
		const row = new ActionRowBuilder()
		.addComponents(
			new ButtonBuilder()
				.setCustomId(buttons.yesButton)
				.setLabel('Ja')
				.setStyle(ButtonStyle.Success),
		)
		.addComponents(
			new ButtonBuilder()
				.setCustomId(buttons.noButton)
				.setLabel('Nein')
				.setStyle(ButtonStyle.Danger),
		)
		.addComponents(
			new ButtonBuilder()
				.setCustomId(buttons.blockButton)
				.setLabel('Frag mich nicht wieder')
				.setStyle(ButtonStyle.Danger),
		);

		await message.reply({ embeds: [embed], components: [row] });

	},
};