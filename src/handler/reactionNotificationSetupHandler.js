const { ActionRowBuilder, ButtonBuilder, ButtonStyle } = require('discord.js');

const { askForReactionNotification } = require('../services/embedService')
const notificationService = require('../services/notificationService')

const buttonPrefix = "reactionNotificationSetup-"

const buttons = {
	yesButton: `${buttonPrefix}YesButton`,
	noButton: `${buttonPrefix}NoButton`,
	blockButton: `${buttonPrefix}BlockButton`,
	saveButton: `${buttonPrefix}SaveButton`
}

const blocklist = "./data/blocklist.json"

module.exports = {
	async execute(interaction, message) {
		if (interaction.customId == buttons.noButton) {
			await interaction.message.delete();
			return
		}

		let embed = askForReactionNotification();

		switch (interaction.customId) {
			case buttons.yesButton:
				/*
			notificationhandler 
			*/
				const row = new ActionRowBuilder()
				emojis.forEach((currentEmoji) => {
					row.addComponents(
						new ButtonBuilder()
							.setCustomId(buttonPrefix + currentEmoji)
							.setLabel(`${currentEmoji}`)
							.setStyle(ButtonStyle.Primary),
					)
				})
				row.addComponents(
					new ButtonBuilder()
						.setCustomId(buttons.saveButton)
						.setLabel("Speichern")
						.setStyle(ButtonStyle.Success)
				)

				await interaction.update({ embeds: [embed], components: [row] })
				break;

			case buttons.blockButton:
				const user = message.author.id
				if (!fs.existsSync(blocklist)) {
					fs.mkdirSync("data")
					fs.writeFileSync(blocklist, "[]")
				}

				// Put userid in blocklist
				allBlockedUsers = JSON.parse(fs.readFileSync((blocklist)))
				allBlockedUsers.push(user)
				fs.writeFileSync(blocklist, JSON.stringify(allBlockedUsers))


				await message.author.send("Okay ich werde dich nicht erneut fragen.")
				await interaction.message.delete()
				break;

			case buttons.saveButton:
				if (choosenEmojis.length > 0) {
					message.react("🗑️")

					// To have a full list of all users who reacted to the message
					// ToDo Users should be removed from this list if they revoke their reaction
					let reactionsWithUsers = {}
					choosenEmojis.forEach(emoji => {
						reactionsWithUsers[emoji] = []
					})

					notificationService.startReactionWatcher(message, reactionsWithUsers)
				}
				await interaction.message.delete()
				break;

			default:
				if (interaction.customId.startsWith(buttonPrefix)) {
					let emote = interaction.customId.split("-")[1]
					choosenEmojis.push(emote)

					// Updating view
					embed.addFields({ name: "Reactions bei welchen benachrichtigt wird.", value: `${choosenEmojis.join(", ")}` })
					await interaction.update({ embeds: [embed] })
				}
				break;

		}
	}
}